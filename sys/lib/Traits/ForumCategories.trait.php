<?php
/**
* @project    Atom-M CMS
* @package    Categories Trait
* @url        https://atom-m.modos189.ru
*/

namespace Traits;

trait ForumCategories {

    protected function getAllCategories() {
        $tree = array();

        // Check register
        if (isset($this->Register[$this->module . '_categories_tree'])) {
            $tree = $this->Register[$this->module . '_categories_tree'];
        } else {
            // Check cache
            if ($this->cached && $this->Cache->check('category_tree_' . $this->cacheKey)) {
                $tree = unserialize($this->Cache->read('category_tree_' . $this->cacheKey));
            } else {
                $tree_temp = $this->DB->query('SELECT a.*, a.parent_forum_id as parent_id, b.children_id as children_id FROM `' .  $this->DB->getFullTableName('forums') . '` as a LEFT JOIN (SELECT parent_forum_id, GROUP_CONCAT(id) as children_id FROM `' .  $this->DB->getFullTableName('forums') . '` GROUP BY parent_forum_id) as b ON b.parent_forum_id = a.id');
                foreach ($tree_temp as $category) {
                    if ($category && is_array($category)) {
                        $category['children_id'] = $category['children_id'] ? explode(',', $category['children_id']) : array();
                        $category['url'] = get_url($this->getModuleURL(($this->module == 'forum' ? 'view_forum/' : 'category/') . $category['id']));
                        $tree[$category['id']] = $category;
                    }
                }
                if (!empty($tree) && count($tree) > 0) {
                    if ($this->cached) {
                        $this->Cache->write(serialize($tree), 'category_tree_' . $this->cacheKey, array('module_' . $this->module, 'category_block'));
                    }
                    $this->Register[$this->module . '_categories_tree'] = $tree;
                }
            }
        }
        return $tree;
    }
    
    /**
     * Get categories chain
     *
     * @param string $cat_id Category/categories ID
     * @return array
     */
    protected function getCategoriesChain($cat_id) {
        $chain = array();
        $tree = $this->getAllCategories();

        if (!empty($tree) && count($tree) > 0) {
            $categories = $cat_id ? explode(',', $cat_id) : array();
            if (count($categories) == 0) { // Without category

            } elseif (count($categories) == 1) { // One category
                $category = $tree[$categories[0]];
                while ($category['parent_id']) {
                    $chain[] = $category;
                    $category = $tree[$category['parent_id']];
                }
                $chain[] = $category;
                $forum_cat = $this->DB->select('forum_cat', DB_FIRST, array('cond' => array('id' => $category['in_cat'])));
                if (count($forum_cat)) {
                    $forum_cat[0]['url']= $this->getModuleURL();
                    $chain[] = $forum_cat[0];
                }
            } else { // Many categories
            }
        }
        return $chain;
    }
}