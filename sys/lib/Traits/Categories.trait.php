<?php
/**
* @project    Atom-M CMS
* @package    Categories Trait
* @url        https://atom-m.modos189.ru
*/

namespace Traits;

trait Categories {

    protected function getChildrenCategories($cat_ids) {
        if (!$cat_ids || (!is_array($cat_ids) && $cat_ids < 1)) return array();

        $cats = array();
        $tree = $this->getAllCategories();
        if (!is_array($cat_ids)) {
            $cat_ids = array($cat_ids);
        }
        foreach ($cat_ids as $id) {
            if ($id && isset($tree[$id])) {
                $cats = array_merge($cats, $tree[$id]['children_id']);
            }
        }

        if ($cats && is_array($cats) && count($cats)) {
            $children_ids = $this->getChildrenCategories(array_unique($cats));
            $cat_ids = array_unique(array_merge((array) $cat_ids, $children_ids));
        }
        return (array) $cat_ids;
    }

	private function getEntriesCount($cat_id) {
        $cat_id = intval($cat_id);
        if ($cat_id < 1) return 0;

        $cat_ids = $this->getChildrenCategories($cat_id);
        if ($cat_ids && is_array($cat_ids) && count($cat_ids)) {
            $entriesModel = \OrmManager::getModelInstance($this->module);
            $query = '';
            foreach($cat_ids as $n => $id) {
                $query .= ($n > 0 ? " OR " : '') . "LOCATE(',".$id.",',CONCAT(',',`category_id`,',')) > 0";
            }
            $total = $entriesModel->getTotal(array('cond' => array($query)));
            return ($total ? $total : 0);
        } else {
            return 0;
        }
    }

    /**
     * Build categories list ({{ categories }})
     *
     * @param mixed $cat_id
     * @return string.
     */
    protected function buildCategoriesMarker($cat_id = false) {
        // Check cache
        if ($this->cached && $this->Cache->check('category_' . $this->cacheKey)) {
            $this->categories = unserialize($this->Cache->read('category_' . $this->cacheKey));
            return;
        }
        // Load category list
        $tree = $this->getAllCategories();

        // Use first category
        $id = is_array($cat_id) ? (count($cat_id) ? $cat_id[0] : false) : $cat_id;
        $id = (!empty($id)) ? ($id < 1 ? false : intval($id)) : false;

        $categories = array();
        // Load children for category or for its parents 
        if ($id && isset($tree[$id])) {
            $category = $tree[$id];
            $children_id = isset($category['children_id']) ? $category['children_id'] : array();
            while (!empty($category['parent_id']) && isset($tree[$category['parent_id']]) && !count($children_id)) {
                $category = $tree[$category['parent_id']];
                $children_id = isset($category['children_id']) ? $category['children_id'] : array();
            }
            if (count($children_id)) {
                foreach ($children_id as $category_id) {
                    if (isset($tree[$category_id])) {
                        $categories[] = $tree[$category_id];
                    }
                }
            }
        }
        // If category doesn't have chilren, load upper level category
        if (!count($categories)) {
            foreach ($tree as $category) {
                if (empty($category['parent_id'])) {
                    $categories[] = $category;
                }
            }
        }

        // Add count of entries if necessory
        if (count($categories)) {
            $calc_count = \Config::read('calc_count', $this->module);
            foreach ($categories as $index => $category) {
                $categories[$index]['entries'] = $calc_count ? $this->getEntriesCount($category['id']) : 0;
            }
        }
        $this->categories = $categories;

        if ($this->cached) $this->Cache->write(serialize($this->categories), 'category_' . $this->cacheKey, array('module_' . $this->module, 'category_block'));
    }

    protected function getAllCategories() {
        $tree = array();

        // Check register
        if (isset($this->Register[$this->module . '_categories_tree'])) {
            $tree = $this->Register[$this->module . '_categories_tree'];
        } else {
            // Check cache
            if ($this->cached && $this->Cache->check('category_tree_' . $this->cacheKey)) {
                $tree = unserialize($this->Cache->read('category_tree_' . $this->cacheKey));
            } else {
                $tree_temp = $this->DB->query('SELECT a.*, b.children_id as children_id FROM `' .  $this->DB->getFullTableName($this->module . '_categories') . '` as a LEFT JOIN (SELECT parent_id, GROUP_CONCAT(id) as children_id FROM `' .  $this->DB->getFullTableName($this->module . '_categories') . '` GROUP BY parent_id) as b ON b.parent_id = a.id');
                foreach ($tree_temp as $category) {
                    if ($category && is_array($category)) {
                        $category['children_id'] = $category['children_id'] ? explode(',', $category['children_id']) : array();
                        $category['url'] = $this->getModuleURL(($this->module == 'forum' ? 'view_forum/' : 'category/') . $category['id']);
                        $tree[$category['id']] = $category;
                    }
                }
                if (!empty($tree) && count($tree) > 0) {
                    if ($this->cached) $this->Cache->write(serialize($tree), 'category_tree_' . $this->cacheKey, array('module_' . $this->module, 'category_block'));
                    $this->Register[$this->module . '_categories_tree'] = $tree;
                }
            }
        }
        return $tree;
    }

    /**
     * Build categories list for select input
     *
     * @param array $cats
     * @param mixed $curr_category
     * @param mixed $id
     * @param string $sep
     * @return string
     */
    protected function buildCategoriesSelector($cats, $curr_category = false, $id = false, $sep = '- ')
    {
        $_curr_category = false;
        if ($curr_category !== false)
            $_curr_category = explode(',', $curr_category);
        $out = '';
        foreach ($cats as $key => $cat) {
            $parent_id = $cat->getParent_id();
            if (($id === false && empty($parent_id)) || (!empty($id) && $parent_id == $id)) {
                $out .= '<option value="' . $cat->getId() . '" '
                . (($_curr_category !== false && array_search($cat->getId(), $_curr_category) !== False) ? "selected=\"selected\"" : "")
                . '>' . $sep . h($cat->getTitle()) . '</option>';

                unset($cats[$key]);

                $out .= $this->buildCategoriesSelector($cats, $curr_category, $cat->getId(), $sep . '- ');
            }
        }

        return $out;
    }
    
    /*
     * Отфильтровывает недоступные пользователю категории из списка категорий $cats
     */
    protected function checkCategories($cats, $id = false, $fieldname = 'no_access') {
        $out = array();
        $method = 'get'.ucfirst($fieldname);
        foreach ($cats as $key => $cat) {
            if (!\ACL::checkAccessInList($cat->$method(),$id))
                $out[] = $cat;
        }
        return $out;
    }
    
    /**
     * Get categories chain
     *
     * @param string $cat_id Category/categories ID
     * @return array
     */
    protected function getCategoriesChain($cat_id) {
        $chain = array();
        $tree = $this->getAllCategories();

        if (!empty($tree) && count($tree) > 0) {
            $categories = $cat_id ? explode(',', $cat_id) : array();
            if (count($categories) == 0) { // Without category

            } elseif (count($categories) == 1) { // One category
                $category = $tree[$categories[0]];
                while ($category['parent_id']) {
                    $chain[] = $category;
                    $category = $tree[$category['parent_id']];
                }
                $chain[] = $category;
            } else { // Many categories
                $start = array();
                $cat_ids = array();
                $ids_length = 0;
                foreach ($categories as $node) {
                    $category = $tree[$node];
                    $start[] = $category;
                    $ids = array();
                    if ($category['parent_id']) {
                        $category = $tree[$category['parent_id']];
                        while ($category['parent_id']) {
                            array_unshift($ids, $category['id']);
                            $category = $tree[$category['parent_id']];
                        }
                        array_unshift($ids, $category['id']);
                    }
                    $cat_ids[] = $ids;
                    $ids_length = count($ids) > $ids_length ? count($ids) : $ids_length;
                }
                for ($n = 0; $n < $ids_length-1; $n++) {
                    if (count($ids) <= $n) break;
                    $equal = true;
                    $id = $ids[$n];
                    foreach ($cat_ids as $ids) {
                        if (count($ids) <= $n || $ids[$n] != $id) {
                            $equal = false;
                            break;
                        }
                    }
                    if (!$equal) {
                        break;
                    } elseif ($id > 0) {
                        array_unshift($chain, $tree[$id]);
                    }
                }
                array_unshift($chain, $start);
            }
        }
        return $chain;
    }
    
    public function getCategoriesByIds($ids) {
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }
        
        $tree = $this->getAllCategories();
        $className = '\\' . ucfirst($this->module) . 'Module\\ORM\\' . ucfirst($this->module) . 'CategoriesEntity';
        $categories = array();
        foreach ($ids as $id) {
            $categories[$id] = new $className(isset($tree[$id]) ? $tree[$id] : array('id' => $id));
        }
        return $categories;
    }
}