<?php
/**
* @project    Atom-M CMS
* @package    Loads Sections Model
* @url        https://atom-m.modos189.ru
*/


namespace LoadsModule\ORM;

class LoadsCategoriesModel extends \OrmModel {
    public $Table = 'loads_categories';
}