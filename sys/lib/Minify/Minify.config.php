<?php 
return array (
  'keywords_reserved' => array(
    'do',
    'if',
    'in',
    'for',
    'let',
    'new',
    'try',
    'var',
    'case',
    'else',
    'enum',
    'eval',
    'null',
    'this',
    'true',
    'void',
    'with',
    'break',
    'catch',
    'class',
    'const',
    'false',
    'super',
    'throw',
    'while',
    'yield',
    'delete',
    'export',
    'import',
    'public',
    'return',
    'static',
    'switch',
    'typeof',
    'default',
    'extends',
    'finally',
    'package',
    'private',
    'continue',
    'debugger',
    'function',
    'arguments',
    'interface',
    'protected',
    'implements',
    'instanceof',
  ),
  'keywords_before' => array(
    'do',
    'in',
    'let',
    'new',
    'var',
    'case',
    'else',
    'enum',
    'void',
    'with',
    'class',
    'const',
    'yield',
    'delete',
    'export',
    'import',
    'public',
    'static',
    'typeof',
    'extends',
    'package',
    'private',
    'continue',
    'function',
    'protected',
    'implements',
    'instanceof',
  ),
  'keywords_after' => array(
    'in',
    'public',
    'extends',
    'private',
    'protected',
    'implements',
    'instanceof',
  ),
  'operators_before' => array(
    '+',
    '-',
    '*',
    '/',
    '%',
    '=',
    '+=',
    '-=',
    '*=',
    '/=',
    '%=',
    '<<=',
    '>>=',
    '>>>=',
    '&=',
    '^=',
    '|=',
    '&',
    '|',
    '^',
    '~',
    '<<',
    '>>',
    '>>>',
    '==',
    '===',
    '!=',
    '!==',
    '>',
    '<',
    '>=',
    '<=',
    '&&',
    '||',
    '!',
    '.',
    '[',
    '?',
    ':',
    ',',
    ';',
    '(',
    '{',
  ),
  'operators_after' => array(
    '+',
    '-',
    '*',
    '/',
    '%',
    '=',
    '+=',
    '-=',
    '*=',
    '/=',
    '%=',
    '<<=',
    '>>=',
    '>>>=',
    '&=',
    '^=',
    '|=',
    '&',
    '|',
    '^',
    '~',
    '<<',
    '>>',
    '>>>',
    '==',
    '===',
    '!=',
    '!==',
    '>',
    '<',
    '>=',
    '<=',
    '&&',
    '||',
    '.',
    '[',
    ']',
    '?',
    ':',
    ',',
    ';',
    '(',
    ')',
    '{',
    '}',
  ),
)
?>