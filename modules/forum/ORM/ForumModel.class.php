<?php
/**
* @project    Atom-M CMS
* @package    Forum Model
* @url        https://atom-m.modos189.ru
*/


namespace ForumModule\ORM;

class ForumModel extends \OrmModel
{
    public $Table = 'forums';

    protected $RelatedEntities = array(
        'themeslist' => array(
            'model' => 'ForumThemes',
            'type' => 'has_many',
            'foreignKey' => 'id_forum',
          ),
        'category' => array(
            'model' => 'ForumCat',
            'type' => 'has_one',
            'foreignKey' => 'id_cat',
        ),
        'last_theme' => array(
            'model' => 'ForumThemes',
            'type' => 'has_one',
            'foreignKey' => 'last_theme_id',
        ),
        'parent_forum' => array(
            'model' => 'Forum',
            'type' => 'has_one',
            'foreignKey' => 'parent_forum_id',
        ),
        'subforums' => array(
            'model' => 'Forum',
            'type' => 'has_many',
            'foreignKey' => 'parent_forum_id',
        ),
    );




    public function getStats()
    {
        $result = getDB()->query("
            SELECT `id` as last_user_id
            , (SELECT `name` FROM `" . getDB()->getFullTableName('users') . "` ORDER BY `puttime` DESC LIMIT 1) as last_user_name
            , (SELECT COUNT(*) FROM `" . getDB()->getFullTableName('posts') . "`) as posts_cnt
            , (SELECT COUNT(*) FROM `" . getDB()->getFullTableName('themes') . "`) as themes_cnt
            FROM `" . getDB()->getFullTableName('users') . "` ORDER BY `puttime` DESC LIMIT 1");
        return $result;
    }


    public function updateForumCounters($id_forum)
    {
        getDB()->query(
            "UPDATE `" . getDB()->getFullTableName($this->Table) . "` SET `themes` =
            (SELECT COUNT(*) FROM `" . getDB()->getFullTableName('themes') . "`
            WHERE `id_forum` = '" . $id_forum . "'), `posts` =
            (SELECT COUNT(b.`id`) FROM `" . getDB()->getFullTableName('themes') . "` a
            LEFT JOIN `" . getDB()->getFullTableName('posts') . "` b ON a.`id`=b.`id_theme`
            WHERE a.`id_forum` = '" . $id_forum . "') - (SELECT COUNT(*) FROM `" . getDB()->getFullTableName('themes') . "`
            WHERE `id_forum` = '" . $id_forum . "'),
            `last_theme_id`=IFNULL((SELECT `id` FROM `" . getDB()->getFullTableName('themes') . "`
            WHERE `id_forum`='" . $id_forum . "'
            ORDER BY `last_post` DESC  LIMIT 1), 0) WHERE `id` = '" . $id_forum . "'" );
    }


    public function updateUserCounters($id_user)
    {
        getDB()->query(
            "UPDATE `" . getDB()->getFullTableName('users') . "` SET
            `themes` = (SELECT COUNT(*) FROM `" . getDB()->getFullTableName('themes') . "`
            WHERE `id_author` = '" . $id_user . "')
            , `posts` = (SELECT COUNT(*) FROM `" . getDB()->getFullTableName('posts') . "`
            WHERE `id_author` = '" . $id_user . "')
            WHERE `id` = '" . $id_user . "'");
    }



    public function upLastPost($from_forum, $id_forum)
    {
        getDB()->query("UPDATE `" . getDB()->getFullTableName($this->Table) . "` as forum SET
            forum.`last_theme_id` = IFNULL((SELECT `id` FROM `" . getDB()->getFullTableName('themes') . "`
            WHERE `id_forum` = forum.`id` ORDER BY `last_post` DESC LIMIT 1), 0)
            WHERE forum.`id` IN ('" . $from_forum . "', '" . $id_forum . "')");
    }


    public function deleteCollisions()
    {
        getDB()->query("DELETE FROM `" . getDB()->getFullTableName('themes')
            . "` WHERE id NOT IN (SELECT DISTINCT id_theme FROM `" . getDB()->getFullTableName('posts') . "`)");
        getDB()->query("DELETE FROM `" . getDB()->getFullTableName('posts')
            . "` WHERE id_theme NOT IN (SELECT DISTINCT id FROM `" . getDB()->getFullTableName('themes') . "`)");
        getDB()->query("DELETE FROM `" . getDB()->getFullTableName('polls')
            . "` WHERE theme_id NOT IN (SELECT DISTINCT id FROM `" . getDB()->getFullTableName('themes') . "`)");
        getDB()->query("DELETE FROM `" . getDB()->getFullTableName('attaches')
            . "` WHERE entity_id NOT IN (SELECT DISTINCT id FROM `" . getDB()->getFullTableName('posts') . "`) AND module = 'forum'");
    }


    public function addLastAuthors($forums)
    {
        $uids = array();
        if (!empty($forums)) {
            foreach ($forums as $forum) {
                if (!$forum->getLast_theme()) continue;

                $uid = $forum->getLast_theme()->getId_last_author();
                if (0 != $uid) {
                    $uids[] = $uid;
                }
            }


            if (!empty($uids)) {
                $uids = implode(', ', $uids);
                $usersModel = \OrmManager::getModelInstance('Users');
                $users = $usersModel->getCollection(array("`id` IN ({$uids})"));


                if (!empty($users)) {
                    foreach ($forums as $forum) {
                        if (!$forum->getLast_theme()) continue;
                        foreach ($users as $user) {
                            if ( $forum->getLast_theme()->getId_last_author() === $user->getId()) {
                                $forum->setLast_author($user);
                            }
                        }
                    }
                }
            }

        }
        return $forums;
    }

    /**
     * @param $user_id
     * @return array|bool
     */
    function __getUserStatistic($user_id) {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        
        $user_id = intval($user_id);
        if ($user_id > 0) {
            $usersModel = \OrmManager::getModelInstance('Users');
            $result = $usersModel->getFirst(array('id' => $user_id));
            if ($result) {
                $res = array();

                if ($result->getThemes() > 0) {
                    $res[] = array(
                        'module' => $module,
                        'name' => 'themes',
                        'text' => __('themes',true,$module),
                        'count' => $result->getThemes(),
                        'url' => get_url("/$module/user_themes/$user_id"),
                    );
                }

                if ($result->getPosts() > 0) {
                    $res[] = array(
                        'module' => $module,
                        'name' => 'posts',
                        'text' => __('messages',true,$module),
                        'count' => $result->getPosts(),
                        'url' => get_url("/$module/user_posts/$user_id"),
                    );
                }
                return $res;
            }
        }
        return false;
    }
    
    /**
     * Count of themes
     */
    function __getEntriesCount() {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        
        $themesModel = \OrmManager::getModelInstance('ForumThemes');
        return array(
            'text' => __('themes',true,$module),
            'count' => $themesModel->getTotal(),
        );
    }
    
    /**
     * Entries for search
     *
     */
    function __getQueriesForSearch() {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        return array(
            "SELECT `message` as `index`, `id` as `entity_id`, 'forumPosts' as `entity_table`, '/view_post/' as `entity_view`, '$module' as `module`, GREATEST(`time`,`edittime`) as `date` FROM `" . getDB()->getFullTableName('posts') . "` WHERE `message` IS NOT NULL AND `message` <> ''",
            "SELECT `title` as `index`, `id` as `entity_id`, 'forumThemes' as `entity_table`, '/view_theme/' as `entity_view`, '$module' as `module`, `time` as `date` FROM `" . getDB()->getFullTableName('themes') . "` WHERE `title` IS NOT NULL AND `title` <> ''",
            "SELECT `description` as `index`, `id` as `entity_id`, 'forumThemes' as `entity_table`, '/view_theme/' as `entity_view`, '$module' as `module`, `time` as `date` FROM `" . getDB()->getFullTableName('themes') . "` WHERE `description` IS NOT NULL AND `description` <> ''",
        );
    }
    
    /**
     * Results for search
     *
     */
    function __correctSearchResults($results) {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        $module_name = '\\' . ucfirst($module) . 'Module\ActionsHandler';
        
        $posts_id = array();
        $themes_id = array();
        
        $view_themes = \ACL::turnUser(array($module, 'view_themes'));
        $view_forums = \ACL::turnUser(array($module, 'view_forums'));
        if (!$view_themes) {
            return array();
        }
        foreach ($results as $key => $result) {
            if ($result->getEntity_table() == 'forumPosts') { // Forum post
                $posts_id[] = $result->getEntity_id();
            } elseif ($result->getEntity_table() == 'forumThemes') { // Title or description forum theme
                $themes_id[] = $result->getEntity_id();
            }
        }
        $posts_id = array_unique($posts_id);
        $themes_id = array_unique($themes_id);
        
        $postsModel = \OrmManager::getModelInstance('forumPosts');
        $postsModel->bindModel('attaches', array(), array('module' => $module));
        $items = count($posts_id) ? $postsModel->getCollection(array('`id` in (' . implode(',', $posts_id) . ')')) : array();
        $posts = array();
        foreach ($items as $post) {
            $posts[$post->getId()] = $post;
        }
        
        $themesModel = \OrmManager::getModelInstance('forumThemes');
        $items = count($themes_id) ? $themesModel->getCollection(array('`id` in (' . implode(',', $themes_id) . ') OR `id` IN (SELECT `id_theme` FROM ' . getDB()->getFullTableName('posts') . ' WHERE `id` IN (' . implode(',', $posts_id) . '))')) : array();
        $themes = array();
        foreach ($items as $theme) {
            $themes[$theme->getId()] = $theme;
        }
        
        $items = count($posts_id) || count($themes_id) ? $this->getCollection(array(
            (count($themes_id) ? '`id` IN (SELECT `id_forum` FROM ' . getDB()->getFullTableName('themes') . ' WHERE `id` IN (' . implode(',', $themes_id) . '))' : '')
            . (count($posts_id) && count($themes_id) ? ' OR ' : '')
            . (count($posts_id) ? '`id` IN (SELECT `id_forum` FROM ' . getDB()->getFullTableName('posts') . ' WHERE `id` IN (' . implode(',', $posts_id) . '))' : '')
        )) : array();
        $forums = array();
        foreach ($items as $forum) {
            $forums[$forum->getId()] = $forum;
        }
        
        $usersModel = \OrmManager::getModelInstance('users');
        $items = count($posts_id) || count($themes_id) ? $usersModel->getCollection(array(
            (count($themes_id) ? '`id` IN (SELECT `id_author` FROM ' . getDB()->getFullTableName('themes') . ' WHERE `id` IN (' . implode(',', $themes_id) . '))' : '')
            . (count($posts_id) && count($themes_id) ? ' OR ' : '')
            . (count($posts_id) ? '`id` IN (SELECT `id_author` FROM ' . getDB()->getFullTableName('posts') . ' WHERE `id` IN (' . implode(',', $posts_id) . '))' : '')
        )) : array();
        $users = array();
        foreach ($items as $user) {
            $users[$user->getId()] = $user;
        }
        
        foreach ($results as $key => &$result) {
            $result->setEntry_url(get_url($result->getModule() . $result->getEntity_view() . $result->getEntity_id()));
            $theme = $result->getEntity_table() == 'forumThemes' && isset($themes[$result->getEntity_id()]) ? $themes[$result->getEntity_id()] : null;
            $result->setAuthor($theme && isset($users[$theme->getId_author()]) ? $users[$theme->getId_author()] : null);
            if ($result->getEntity_table() == 'forumPosts' && isset($posts[$result->getEntity_id()])) { // Forum post
                $post = $posts[$result->getEntity_id()];
                if ($post) {
                    $result->setAuthor(isset($users[$post->getId_author()]) ? $users[$post->getId_author()] : null);
                    $result->setAttaches($post->getAttaches());
                    $theme = isset($themes[$post->getId_theme()]) ? $themes[$post->getId_theme()] : null;
                }
            }
            if ($theme) {
                if (!$module_name::checkAccessForum(array('view_themes',$theme->getId_forum()))) {
                    unset($results[$key]);
                }
                if ($view_forums && $module_name::checkAccessForum(array('view_forums',$theme->getId_forum())) && isset($forums[$theme->getId_forum()])) {
                    $forum = $forums[$theme->getId_forum()];
                    if ($forum) {
                        $result->setForum_title($forum->getTitle());
                        $result->setForum_url(get_url($result->getModule() . '/view_forum/' . $forum->getId()));
                    }
                }
                $result->setTitle($theme->getTitle());
            }
        }
        return $results;
    }
    
    /**
     * List of links for Sitemap
     *
     */
    function __getSitemapList($host) {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        $urls = array();
        $themes = getDB()->select('themes', DB_ALL);
        if (count($themes)) {
            foreach ($themes as $theme) {
                $urls[] = "$host$module/view_theme/" . $theme['id'];
            }
        }
        return $urls;
    }
}
