<?php
return array(
    'icon_class' => 'mdi-action-language',
    'pages'   => array(
        WWW_ROOT.'/admin/settings.php?m=pages' => __('Settings'),
        WWW_ROOT.'/admin/pages/page.php'       => __('Pages editor',false,'pages'),
    ),
);