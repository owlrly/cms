<?php
/**
* @project    Atom-M CMS
* @package    News Sections Model
* @url        https://atom-m.modos189.ru
*/


namespace NewsModule\ORM;

class NewsCategoriesModel extends \OrmModel {
    public $Table = 'news_categories';
}