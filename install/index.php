<?php

if (basename(dirname(__FILE__)) != "install") {
    header('Location: /');
    die();
}

include_once '../sys/boot.php';

$Viewer = new \Viewer_Manager(['template_path' => ROOT . '/install/template/html/', 'layout' => false]);
$output = $Viewer->parseTemplate('index.html.twig', array());

echo($output);
