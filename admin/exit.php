<?php
/**
* @project    Atom-M CMS
* @package    Admin Panel module
* @url        https://atom-m.modos189.ru
*/


include_once '../sys/boot.php';
include_once ROOT . '/admin/inc/adm_boot.php';


if (isset($_SESSION['adm_panel_authorize'])) unset($_SESSION['adm_panel_authorize']);
redirect('/');


